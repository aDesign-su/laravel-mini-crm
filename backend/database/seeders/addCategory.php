<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Телефоны',
                'code' => 'phones'
            ],
            [
                'name' => 'Перефирия',
                'code' => 'paraphyria'
            ],
            [
                'name' => 'Техника',
                'code' => 'technic'
            ],
        ]);
    }
}
