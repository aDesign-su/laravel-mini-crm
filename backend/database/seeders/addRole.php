<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
            [
                'status' => 'Пользователь',
                'role' => 0
            ],
            [
                'status' => 'Редактор',
                'role' => 1
            ],
            [
                'status' => 'Администратор',
                'role' => 2
            ],
        ]);
    }
}
