<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('params')->insert([
            [
                'title' => 'Цвет',
                'description' => 'Красный',
                'code_product' => 'a4c10987',
                'code_manufacturer' => '4606453849072',
                'price' => '15850',
                'category_id' => 1,
            ],
            [
                'title' => 'Экран',
                'description' => 'IPS',
                'code_product' => 'a4g67887',
                'code_manufacturer' => '4606453849072',
                'price' => '35850',
                'category_id' => 1,
            ],
            [
                'title' => 'Экран',
                'description' => 'IPS',
                'code_product' => 'sh767887',
                'code_manufacturer' => '4606453849072',
                'price' => '35850',
                'category_id' => 3,
            ],
        ]);
    }
}
