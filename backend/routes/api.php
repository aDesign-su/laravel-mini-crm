<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\Api\Auth\GoogleAuthController;
Route::post('auth/google', [GoogleAuthController::class, 'authenticateWithGoogle']);
Route::post('auth/google', [GoogleAuthController::class, 'authenticateWithGoogle'])->withoutMiddleware('auth');


Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('params', 'Api\ParamsController');
Route::resource('info', 'Api\InfoController');
Route::resource('leads', 'Api\LeadController');
Route::resource('statuses', 'Api\StatusController');
Route::resource('categories', 'Api\CategoryController');
Route::resource('users', 'Api\UserListController');
Route::resource('roleList', 'Api\RoleController');

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::get('user', [AuthController::class, 'user']);
    Route::post('logout', 'AuthController@logout');
    Route::post('register', 'AuthController@register');
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', 'AuthController@me');
});
