<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Services\UserList\UserListService;
use App\Services\Response\ResponseService;
use Illuminate\Http\Request;

class UserListController extends ApiController
{
    public function __construct(UserListService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ResponseService::sendJsonResponse(
            true,
            [
                'user'=>$this->service->getList()->toArray(),
            ],
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Users $user)
    {
        $user = $this->service->store($request, $user);
        return ResponseService::sendJsonResponse(
            true,
            [
                'user' => $user->toArray()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Users $user)
    {
        return ResponseService::sendJsonResponse(
            true,
            [
                'user' => $user->toArray(),
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $user)
    {
        //
        $user = $this->service->store($request, $user);
        return ResponseService::sendJsonResponse(
            true,
            [
                'user' => $user->toArray()
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $user)
    {
        //
        $this->service->destroy($user);

        return ResponseService::sendJsonResponse(
            true,
            [
                'message' => 'User deleted successfully.'
            ]
        );
    }
}
