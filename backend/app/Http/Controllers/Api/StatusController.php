<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Status;
use App\Services\Response\ResponseService;
use App\Services\Status\StatusService;
use Illuminate\Http\Request;

class StatusController extends ApiController
{
    public function __construct(StatusService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return ResponseService::sendJsonResponse(
            true,
            [
                'items'=>$this->service->getItems()->toArray()
            ],
//            [
//                '/api/statuses',
//                '/api/categories'
//            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Status $status)
    {
        $request->validate([
            'title' => 'required|string',
        ]);
        $status = $this->service->store($request, $status);
        return ResponseService::sendJsonResponse(
            true,
            [
                'items' => $status->toArray()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Status $status)
    {
        //
        return ResponseService::sendJsonResponse(
            true,
            [
                'items' => $status->toArray()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        //
        $this->service->destroy($status);

        return ResponseService::sendJsonResponse(
            true,
            [
                'message' => 'Category deleted successfully.'
            ],
        );
    }
}
