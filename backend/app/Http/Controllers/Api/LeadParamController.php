<?php

namespace App\Http\Controllers\Api;

use App\Models\LeadParam;
use App\Services\Params\InfoService;
use App\Services\Response\ResponseService;
use Illuminate\Http\Request;

class LeadParamController extends ApiController
{
    public function __construct(InfoService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LeadParam $LeadParam)
    {
        //
        $LeadParam = $this->service->store($request, $LeadParam);
        return ResponseService::sendJsonResponse(
            true,
            [
                'param' => $LeadParam->toArray()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeadParam  $leadParam
     * @return \Illuminate\Http\Response
     */
    public function show(LeadParam $leadParam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeadParam  $leadParam
     * @return \Illuminate\Http\Response
     */
    public function edit(LeadParam $leadParam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeadParam  $leadParam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeadParam $leadParam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeadParam  $leadParam
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeadParam $leadParam)
    {
        //
    }
}
