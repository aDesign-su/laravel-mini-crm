<?php

namespace App\Http\Controllers\Api;

use App\Models\Param;
use App\Http\Controllers\Controller;
use App\Services\Params\ParamListService;
use App\Services\Response\ResponseService;
use Illuminate\Http\Request;

class ParamsController extends ApiController
{
    public function __construct(ParamListService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return ResponseService::sendJsonResponse(
            true,
            [
                'param' => $this->service->getParams()->toArray()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Param $param)
    {
        //
        $param = $this->service->store($request,$param);
        return ResponseService::sendJsonResponse(
            true,
            [
                'param' => $param->toArray()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Param $param)
    {
        //
        return ResponseService::sendJsonResponse(
            true,
            [
                'param' => $param->toArray()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Param $param)
    {
        //
        $this->service->destroy($param);
        return ResponseService::sendJsonResponse(
            true,
            [
                'message' => 'Lead deleted successfully.'
            ],
        );
    }
}
