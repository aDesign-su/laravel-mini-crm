<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Google_Client;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class GoogleAuthController extends Controller
{
    public function authenticateWithGoogle(Request $request)
    {
        $client = new Google_Client(['client_id' => env('GOOGLE_CLIENT_ID')]);  // Установка client_id из Google Console
        $idToken = $request->input('idToken');

        $payload = $client->verifyIdToken($idToken);
        if ($payload) {
            $googleId = $payload['sub'];  // Уникальный идентификатор пользователя Google
            $email = $payload['email'];
            $name = $payload['name'];
            $avatar = $payload['picture'];

            // Укажите идентификатор роли по умолчанию, например, 2 для "обычного пользователя"
            $roleId = 1; // Или другой идентификатор роли

            // Поиск пользователя или создание нового
            $user = User::firstOrCreate(
                ['email' => $email],
                [
                    'name' => $name,
                    'google_id' => $googleId,
                    'avatar' => $avatar,
                    'role_id' => $roleId  // Добавляем роль при создании пользователя
                ]
            );

            // Аутентификация пользователя
            Auth::login($user);

            // Генерация токена для дальнейших запросов
            $token = $user->createToken('authToken')->accessToken;

            // Перенаправление на страницу home
            return response()->json([
                'token' => $token,
                'user' => $user
            ]);
        } else {
            return response()->json(['error' => 'Invalid Google token'], 401);
        }
    }
}

