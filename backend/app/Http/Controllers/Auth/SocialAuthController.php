<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SocialAuthController extends Controller
{
    // Перенаправляем пользователя на Google для авторизации
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    // Обработка ответа от Google
    public function handleGoogleCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
            $user = $this->findOrCreateUser($googleUser);

            Auth::login($user);
            return redirect()->to('/home');
        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    // Создаем пользователя, если он не существует, или возвращаем существующего
    public function findOrCreateUser($googleUser)
    {
        $user = User::where('email', $googleUser->getEmail())->first();

        if ($user) {
            return $user;
        }

        return User::create([
            'name' => $googleUser->getName(),
            'email' => $googleUser->getEmail(),
            'google_id' => $googleUser->getId(),
            'avatar' => $googleUser->getAvatar(),
            'password' => bcrypt(str_random(16)), // Генерация случайного пароля
        ]);
    }
}

