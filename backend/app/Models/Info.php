<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;

    protected $table = 'lead_param';
    protected $fillable = [
        'lead_id',
        'param_id',
    ];
//    protected $casts = [
//        'param_id' => 'array',
//    ];
}
