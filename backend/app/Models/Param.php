<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Param extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'code_product',
        'code_manufacturer',
        'price',
        'category_id',
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
