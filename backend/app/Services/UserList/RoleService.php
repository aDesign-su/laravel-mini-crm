<?php

namespace App\Services\UserList;

use App\Models\Role;

class RoleService
{

    public function getList()
    {
        return Role::all();
    }
}
