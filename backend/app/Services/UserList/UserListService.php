<?php

namespace App\Services\UserList;

use App\Models\Users;
use Illuminate\Http\Request;

class UserListService
{
    public function store(Request $request, Users $user)
    {
        $user->fill($request->only($user->getFillable()));
        $user->save();
        return $user;
    }
    public function getList()
    {
        return Users::with('role')->get();
    }

    public function destroy(Users $user)
    {
        $user->delete();
    }
}
