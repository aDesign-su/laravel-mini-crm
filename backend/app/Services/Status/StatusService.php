<?php

namespace App\Services\Status;

use App\Models\Status;

class StatusService
{

    public function getItems()
    {
        return Status::all();
    }

    public function destroy(Status $status)
    {
        $status->delete();
    }

    public function store(\Illuminate\Http\Request $request, Status $status)
    {
        $status->fill($request->only($status->getFillable()));
        $status->save();
        return $status;
    }
}
