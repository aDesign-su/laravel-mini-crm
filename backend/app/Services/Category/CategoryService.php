<?php

namespace App\Services\Category;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryService
{
    public function getItems()
    {
        return Category::all();
    }
    public function store(Request $request, Category $cat)
    {
        $cat->fill($request->only($cat->getFillable()));
        $cat->save();
        return $cat;
    }
    public function destroy(Category $category)
    {
        $category->delete();
    }
}
