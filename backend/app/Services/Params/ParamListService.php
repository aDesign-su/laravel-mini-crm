<?php

namespace App\Services\Params;

use App\Models\Param;
use Illuminate\Http\Request;

class ParamListService
{
    public function getParams()
    {
        return Param::with('category')->get();
    }
    public function store(Request $request, Param $param)
    {
        $param->fill($request->only($param->getFillable()));
        $param->save();
        return $param;
    }
    public function destroy(Param $param)
    {
        $param->delete();
    }
}
