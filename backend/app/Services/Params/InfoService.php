<?php

namespace App\Services\Params;

use App\Models\Info;
use Illuminate\Http\Request;

class InfoService
{
    public function getItems()
    {
        return Info::all();
    }
    public function store(Request $request, Info $info)
    {
        $info->fill($request->only($info->getFillable()));
        $info->save();
        return $info;
    }
    public function destroy(Info $info)
    {
        $info->delete();
    }
}
