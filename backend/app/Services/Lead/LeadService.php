<?php

namespace App\Services\Lead;

use App\Models\Lead;
use Illuminate\Http\Request;

class LeadService
{
    public function store(Request $request, Lead $lead)
    {
        $lead->fill($request->only($lead->getFillable()));
        $lead->save();
        return $lead;
    }
    public function getItems()
    {
        return Lead::with('category', 'status', 'param')->get();
    }

    public function destroy(Lead $lead)
    {
        $lead->delete();
    }
}
